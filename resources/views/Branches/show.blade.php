<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Dpd pick ups</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">

        <!-- Styles -->
        <style>
            html, body {
                background-color: #fff;
                color: #636b6f;
                font-family: 'Nunito', sans-serif;
                font-weight: 200;
                height: 100vh;
                margin: 0;
            }

            .full-height {
                height: 100vh;
            }

            .flex-center {
                align-items: center;
                display: flex;
                justify-content: center;
            }

            .position-ref {
                position: relative;
            }

            .top-right {
                position: absolute;
                right: 10px;
                top: 18px;
            }

            .content {
                text-align: center;
            }

            .title {
                font-size: 84px;
            }

            .links > a {
                color: #636b6f;
                padding: 0 25px;
                font-size: 13px;
                font-weight: 600;
                letter-spacing: .1rem;
                text-decoration: none;
                text-transform: uppercase;
            }

            .m-b-md {
                margin-bottom: 30px;
            }
        </style>
    </head>
    <body>
        <div class="flex-upper position-ref full-height">
            <div class="content">
                @if (isset($branch))
                    <div class="title m-b-md">
                        {{$branch->address->company}}
                    </div>
                    <div class="links">
                        <table class="table-bordered flex-center">
                            <tr>
                                <th>Adresa</th>
                                <th>GPS súradnice</th>
                            </tr>
                            <tr>
                                <td>{{$branch->address->street . ' ' . $branch->address->house_number }}</td>
                                <td>{{'N' .$branch->location->latitude.'°'}}</td>
                            </tr>
                            <tr>
                                <td>{{$branch->address->postcode . ' ' . $branch->address->city}}</td>
                                <td>{{'E' .$branch->location->longitude.'°'}}</td>
                            </tr>
                        </table>

                        <table class="table-bordered">
                            <tr>
                                <th>Den</th>
                                <th>Dopoledne</th>
                                <th>Odpoledne</th>
                            </tr>
                            @foreach($branch->hours as $hour)
                                <tr>
                                    <td>{{$hour->dayOfWeek}}</td>
                                    <td>{{json_decode($hour->businessHour)->hoursMorning}}</td>
                                    <td>{{json_decode($hour->businessHour)->hoursAfternoon}}</td>
                                </tr>
                            @endforeach
                        </table>
                    </div>
                    @elseif (isset($error))
                    <div class="title m-b-md">
                        {{$error}}
                    </div>
                @endif
            </div>
        </div>
    </body>
</html>
