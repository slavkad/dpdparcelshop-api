<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Dpd pick ups</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">

        <!-- Styles -->
        <style>
            html, body {
                background-color: #fff;
                color: #636b6f;
                font-family: 'Nunito', sans-serif;
                font-weight: 200;
                height: 100vh;
                margin: 0;
            }

            .full-height {
                height: 100vh;
            }

            .flex-center {
                align-items: center;
                display: flex;
                justify-content: center;
            }

            .position-ref {
                position: relative;
            }

            .top-right {
                position: absolute;
                right: 10px;
                top: 18px;
            }

            .content {
                text-align: center;
            }

            .title {
                font-size: 84px;
            }

            .links > a {
                color: #636b6f;
                padding: 0 25px;
                font-size: 13px;
                font-weight: 600;
                letter-spacing: .1rem;
                text-decoration: none;
                text-transform: uppercase;
            }

            .m-b-md {
                margin-bottom: 30px;
            }
        </style>
    </head>
    <body>
        <div class="flex-upper position-ref full-height">
            <div class="content">
                <div class="title m-b-md">
                    PickUp Branches
                </div>
                @if (count($branches) >= 1 )
                <div class="links flex-center">
                    <table class="links">
                        <tr>
                            <th>Branch</th>
                            <th>Address</th>
                            <th>Phone number</th>
                        </tr>
{{--                        {{dd($branches)}}--}}
                        @foreach ($branches as $key => $branch)
                        <tr>
                            <td><a href="{{$url = action('WebControllers\BranchController@show', ['id' => $key])}}">{{$branch->address->company}}</a></td>
                            <td>{{$branch->address->street . ', '. $branch->address->city}}</td>
                            <td>{{$branch->phoneNumber}}</td>
                        </tr>
                        @endforeach
                    </table>
                </div>
                    <div>
                        @if ($page > 1)
                            <a href="{{$url = action('WebControllers\BranchController@index', ['page' => 1])}}">First Page</a>
                            <a href="{{$url = action('WebControllers\BranchController@index', ['page' => $page-1])}}">Previous Page</a>
                        @endif
                        @if ($page < $lastPage)
                            <a href="{{$url = action('WebControllers\BranchController@index', ['page' => $page+1])}}">Next Page</a>
                            <a href="{{$url = action('WebControllers\BranchController@index', ['page' => $lastPage])}}">Last Page</a>
                            @endif
                    </div>
                @else 'No records'
                @endif
            </div>
        </div>
    </body>
</html>
