<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class BranchResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
          'id' => $this->id,
          'internalId' => $this->internalId,
          'internalName' => $this->company,
          'location' => $this->location,
          'address'=> $this->address,
          'businessHours' => $this->getHoursAttribute(),
            'phoneNumber' => $this->phoneNumber,
          'email' => $this->email,
            'fax' => $this->fax,
            'homepage' => $this->homepage,
            'pickup_allowed' => $this->pickup_allowed,
            'return_allowed'=> $this->return_allowed,
            'express_allowed'=> $this->express_allowed,
        'cardpayment_allowed'=> $this->cardpayment_allowed,
            'service'=> $this->service,
            'photo'=> $this->photo
        ];

    }

}
