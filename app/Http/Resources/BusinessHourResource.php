<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class BusinessHourResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'dayOfWeek'=> $this->dayOfWeek,
            'businessHour' => $this->businessHour
        ];

        return parent::toArray($request);
    }
}
