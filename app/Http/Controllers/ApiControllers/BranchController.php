<?php

namespace App\Http\Controllers\ApiControllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Http\Resources\BranchCollection;
use App\Http\Resources\BranchResource;
use App\Model\Branch;
use Illuminate\Http\Response;

class BranchController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function index()
    {
        $branch = Branch::hydrate(Branch::getAll());
        return BranchCollection::collection(collect($branch));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Model\Coordinates $coordinates
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try {
            $branchArray = Branch::getAll();
            if (isset($branchArray[$id])) {

                $branch = new Branch(Branch::getAll()[$id]);
                return
                  (new BranchResource($branch))
                  ->response()
                  ->setStatusCode(200);
            }else{
                return (new Response("PickUp Shop with this id doesn't exist", 404) );

            }

        }catch(\Exception $exception){

            return (new Response("Unknow error", 404) );
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function edit(Branch $branch)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Branch $branch)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy(Branch $branch)
    {
        //
    }
}

