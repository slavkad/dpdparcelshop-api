<?php

namespace App\Http\Controllers\WebControllers;

use App\Model\Branch;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class BranchController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function index(Request $request)
    {
        $perPage = 15;
        $page = $request->input('page', 1);

        $branches = Collection::make(Branch::hydrate(Branch::getAll()));
        return view(
            'Branches/index', [
            'branches' => (Collection::make($branches)->forPage($page, $perPage)->all()),
            'lastPage' => ceil(count($branches)/$perPage),
            'page' => $page
            ]
        );
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Model\CoordinatesModel $coordinatesModel
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try {
            $branchArray = Branch::getAll();
            if (isset($branchArray[$id])) {

                $branch = new Branch($branchArray[$id]);
                return view(
                    'Branches/show', [
                    'branch' => $branch
                    ]
                );
            }

            return view(
                'Branches/show', [
                'error' => 'No branch with this id'
                ]
            );
        }catch(\Exception $e){
            throw new NotFoundHttpException();
        }

    }

}
