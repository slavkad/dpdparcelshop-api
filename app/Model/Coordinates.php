<?php

/**
 * @file Coordinates.php
 * Coordinates model for map
 */
declare (strict_types=1);

namespace App\Model;

/**
 * Class Coordinates
 *
 * @package App\Model
 */
class Coordinates extends ApiModel
{
    protected $attributes = [
      'longitude' => '',
      'latitude' => '',
    ];

    protected $fillable = [
      'latitude',
      'longitude',
    ];

    protected $visible = [
      'latitude' => 'latitude',
      'longitude' => 'longitude',
    ];

    /*
     * @float $longitude
     */
    protected $longitude;

    /*
     * @float $longitude
     */
    protected $latitude;

    /**
     * @return mixed
     */
    public function getLongitudeAttribute()
    {

        return $this->longitude;
    }

    /**
     * @param mixed $longitude
     */
    public function setLongitudeAttribute($longitude): void
    {

        $this->longitude = filter_var(
          $longitude,
          FILTER_SANITIZE_NUMBER_FLOAT,
          ['flags' => FILTER_FLAG_ALLOW_FRACTION, 'options' => '.']
        );
    }

    /**
     * @return mixed
     */
    public function getLatitudeAttribute()
    {

        return $this->latitude;
    }

    /**
     * @param mixed $latitude
     */
    public function setLatitudeAttribute($latitude): void
    {

        $this->latitude = filter_var(
          $latitude,
          FILTER_SANITIZE_NUMBER_FLOAT,
          ['flags' => FILTER_FLAG_ALLOW_FRACTION, 'options' => '.']
        );
    }
}
