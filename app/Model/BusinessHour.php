<?php

declare(strict_types=1);

namespace App\Model;

class BusinessHour extends ApiModel
{
    protected static $appendsToModel = [
      'dayOfWeek' => 'dayOfWeek',
      'businessHour' => 'businessHour',
    ];

    protected $attributes = [
      'dayOfWeek' => '',
      'businessHour' => '',
    ];
    protected $fillable = [
      'day',
      'dayName' => 'dayName',
      'openMorning',
      'closeMorning',
      'openAfternoon',
      'closeAfternoon',
      'dayOfWeek',
      'businessHour',
    ];

    protected $visible = [
      'day' => 'day',
      'dayOfWeek' => 'dayOfWeek',
      'businessHour' => 'businessHour',
    ];

    protected $appends = [
      'dayOfWeek',
      'businessHour',
    ];
    protected $day;
    protected $dayOfWeek;
    protected $businessHour;

    /**
     * @return mixed
     */
    public function getDayAttribute()
    {

        return $this->day;
    }

    /**
     * @param mixed $day
     */
    public function setDayAttribute($day): void
    {

        $this->day = $day;
    }

    /**
     * @return string
     */
    public function getDayOfWeekAttribute(): string
    {

        return $this->dayOfWeek;
    }

    /**
     * @return string
     */
    public function getBusinessHourAttribute(): string
    {

        return $this->businessHour;
    }

    /**
     * @param mixed $businessHour
     */
    public function setBusinessHourAttribute($businessHour): void
    {

        $this->businessHour = json_encode(
          [
            'hoursMorning' => $this->openMorning.' - '.$this->closeMorning,
            'hoursAfternoon' => $this->openAfternoon.' - '.$this->closeAfternoon,
          ]
        );
    }

    /**
     * @param mixed $dayName
     */
    public function setDayOfWeekAttribute($dayName): void
    {

        $this->dayOfWeek = $this->dayName;
    }
}
