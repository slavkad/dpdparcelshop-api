<?php

declare(strict_types=1);

namespace App\Model;

class Address extends ApiModel
{
    protected $attributes = [
      'company' => '',
      'city' => '',
      'street' => '',
      'house_number' => '',
      'postcode' => '',
    ];
    protected $fillable = [
      'company',
      'city',
      'street',
      'house_number',
      'postcode',
    ];

    protected $visible = [
      'company' => 'company',
      'city' => 'city',
      'street' => 'street',
      'house_number' => 'house_number',
      'postcode' => 'postcode',
    ];

    protected $company;
    protected $street;
    protected $house_number;
    protected $postcode;
    protected $city;

    /**
     * @return mixed
     */
    public function getCompanyAttribute()
    {

        return $this->company;
    }

    /**
     * @param mixed $company
     */
    public function setCompanyAttribute($company): void
    {

        $this->company = $company;
    }

    /**
     * @return mixed
     */
    public function getStreetAttribute()
    {

        return $this->street;
    }

    /**
     * @param mixed $street
     */
    public function setStreetAttribute($street): void
    {

        $this->street = $street;
    }

    /**
     * @return mixed
     */
    public function getHouseNumberAttribute()
    {

        return $this->house_number;
    }

    /**
     * @param mixed $house_number
     */
    public function setHouseNumberAttribute($house_number): void
    {

        $this->house_number = $house_number;
    }

    /**
     * @return mixed
     */
    public function getPostcodeAttribute()
    {

        return $this->postcode;
    }

    /**
     * @param mixed $postcode
     */
    public function setPostcodeAttribute($postcode): void
    {

        $this->postcode = $postcode;
    }

    /**
     * @return mixed
     */
    public function getCityAttribute()
    {

        return $this->city;
    }

    /**
     * @param mixed $city
     */
    public function setCityAttribute($city): void
    {

        $this->city = $city;
    }

}
