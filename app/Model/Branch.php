<?php

declare(strict_types=1);

namespace App\Model;

class Branch extends ApiModel
{

    protected static $appendsToModel = [
      'address' => 'address',
      'location' => 'location',
      'internalId' => 'internalId',
      'internalName' => 'internalName',
      'phoneNumber' => 'phoneNumber',
    ];
    public $attributes = [];

    protected $fillable = [
      'id',
      'internalId',
      'internalName',
      'phone',
      'phoneNumber',
      'fax',
      'email',
      'homepage',
      'pickup_allowed',
      'return_allowed',
      'express_allowed',
      'cardpayment_allowed',
      'street',
      'city',
      'house_number',
      'postcode',
      'address',
      'company',
      'hours',
      'location',
      'longitude',
      'latitude',
      'service',
      'photo',
      'businessHours',
    ];
    protected $visible = [
      'id',
      'internalId',
      'phoneNumber',
      'email',
      'homepage',
      'pickup_allowed',
      'return_allowed',
      'express_allowed',
      'cardpayment_allowed',
      'internalName',
      'address',
      'hours',
      'location',
      'service',
      'photo',
      'businessHours',
    ];
    protected $appends = [
      'internalName' => 'internalName',
      'phoneNumber' => 'phoneNumber',
    ];
    /**
     * @var string
     */
    protected $id;
    protected $internalId;
    /**
     * @var string
     */
    protected $internalName;
    /**
     * @var \App\Model\Coordinates
     */
    protected $location;
    /**
     * @var array|BusinessHour[]
     */
    protected $businessHours;
    /**
     * @var \App\Model\Address
     */
    protected $address;
    /**
     * @var string
     */
    protected $phoneNumber;
    /**
     * @var string
     */
    protected $email;
    protected $homepage;
    protected $pickup_allowed;
    protected $return_allowed;
    protected $service;
    protected $photo;
    protected $express_allowed;
    protected $cardpayment_allowed;

    public function __construct(array $attributes = [])
    {

        parent::__construct($attributes);
        self::$instances[] = $this;
    }

    /**
     * @return string
     */
    public function getIdAttribute(): string
    {

        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getHomepageAttribute()
    {

        return $this->homepage;
    }

    /**
     * @param string $homepage
     */
    public function setHomepageAttribute(string $homepage): void
    {

        $this->homepage = $homepage;
    }

    /**
     * @return mixed
     */
    public function getServiceAttribute()
    {

        return $this->service;
    }

    /**
     * @param int $service
     */
    public function setServiceAttribute(int $service): void
    {

        $this->service = $service;
    }

    /**
     * @return mixed
     */
    public function getPhotoAttribute()
    {

        return $this->photo;
    }

    /**
     * @param string $photo
     */
    public function setPhotoAttribute(string $photo): void
    {

        $this->photo = $photo;
    }

    /**
     * @return mixed
     */
    public function getPickupAllowedAttribute()
    {

        return $this->pickup_allowed;
    }

    /**
     * @param int $pickup_allowed
     */
    public function setPickupAllowedAttribute(int $pickup_allowed): void
    {

        $this->pickup_allowed = $pickup_allowed;
    }

    /**
     * @return mixed
     */
    public function getReturnAllowedAttribute()
    {

        return $this->return_allowed;
    }

    /**
     * @param int $return_allowed
     */
    public function setReturnAllowedAttribute(int $return_allowed): void
    {

        $this->return_allowed = $return_allowed;
    }

    /**
     * @return mixed
     */
    public function getExpressAllowedAttribute()
    {

        return $this->express_allowed;
    }

    /**
     * @param int $express_allowed
     */
    public function setExpressAllowedAttribute(int $express_allowed): void
    {

        $this->express_allowed = $express_allowed;
    }

    /**
     * @return mixed
     */
    public function getCardpaymentAllowedAttribute()
    {

        return $this->cardpayment_allowed;
    }

    /**
     * @param int $cardpayment_allowed
     */
    public function setCardpaymentAllowedAttribute(int $cardpayment_allowed): void
    {

        $this->cardpayment_allowed = $cardpayment_allowed;
    }

    /**
     * @return string
     */
    public function getInternalIdAttribute(): string
    {

        return $this->internalId;
    }

    /**
     * @return \App\Model\Coordinates
     */
    public function getLocationAttribute(): Coordinates
    {

        return $this->location;
    }

    /**
     * @return \App\Model\BusinessHour[]|array
     */
    public function getHoursAttribute()
    {

        return $this->businessHours;
    }

    /**
     * @return string
     */
    public function getPhoneNumberAttribute(): string
    {

        return $this->phoneNumber;
    }

    /**
     * @return string
     */
    public function getEmailAttribute(): string
    {

        return $this->email;
    }

    /**
     * @param string $internalId
     */
    public function setIdAttribute(string $id): void
    {

        $this->id = $id;
        $this->internalId = $this->id;
    }

    /**
     * No param because we're using another attribute
     */
    public function setInternalNameAttribute(): void
    {

        $this->internalName = $this->company;
    }

    /**
     * @param \App\Model\Coordinates $location
     */
    public function setLocationAttribute(): void
    {

        $loc = Coordinates::hydrate(
          ['location' => ['latitude' => $this->latitude, 'longitude' => $this->longitude]]
        );
        $this->location = $loc['location'];
    }

    /**
     * @param \App\Model\BusinessHour[]|array $businessHours
     */
    public function setHoursAttribute(array $hours): void
    {

        $this->businessHours = BusinessHour::hydrate($hours);
    }

    /**
     * @param string $phoneNumber
     */
    public function setPhoneNumberAttribute(string $phone): void
    {

        $this->phoneNumber = $this->phone;
    }

    /**
     * @param string $email
     */
    public function setEmailAttribute(string $email): void
    {

        $this->email = $email;
    }

    /**
     * @return \App\Model\Address
     */
    public function getAddressAttribute()
    {

        return $this->address;
    }

    /**
     * @param \App\Model\Address $address
     */
    public function setAddressAttribute(): void
    {

        $address = Address::hydrate(
          [
            'address' => [
              'city' => $this->city,
              'company' => $this->company,
              'street' => $this->street,
              'house_number' => $this->house_number,
              'postcode' => $this->postcode,
            ],
          ]
        );
        $this->address = $address['address'];
    }
}
