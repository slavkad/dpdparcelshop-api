<?php

/**
 * @file MassAssignmentException.php
 */
declare(strict_types=1);

namespace App\Model;

use RuntimeException;

/**
 * Class MassAssignmentException
 *
 * @ App\Model
 */
class MassAssignmentException extends RuntimeException
{

}