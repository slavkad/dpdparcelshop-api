<?php

declare(strict_types=1);

namespace App\Services;

use Exception;
use GuzzleHttp\Client;
use GuzzleHttp\Psr7\Request;
use GuzzleHttp\Psr7\Response;
use JsonSerializable;

class DpdClient
{
    protected $repository;

    protected $headers;

    protected $request;

    protected $response;

    public function __construct()
    {
        $this->repository = app(Client::class);

        $this->headers = [
          'Accept' => 'application/json',
          'Content-type' => 'application/json'
        ];

        $this->request = new Request('GET', config('services.dpdparcel.endpoint'), $this->headers);
    }

    private function makeRequest() : Response
    {

        $response = $this->repository->send($this->request, []);
        if ($response->getStatusCode() != 200) {

            throw new Exception($response->getReasonPhrase());
        }

        return $response;
    }

    public function getResponse()
    {
        $response = $this->makeRequest();
        $answer = json_decode($response->getBody()->getContents(), true);
        if (isset($answer->code) && $answer->code != 200){

            throw new Exception($answer->message);
        }

        return $answer;
    }

    public function getItems()
    {

        return $this->getResponse()['data']['items'];

    }
}


