<?php

namespace Tests\Feature;

use App\Services\DpdClient;
use Tests\TestCase;

class DpdTest extends TestCase
{
    /**
     * Fetch data from Dpd parcelshop API
     *
     * @return void
     */
    public function testHasData()
    {

        $dpd = new DpdClient();
        $answer = $dpd->getResponse();
        $this->assertObjectHasAttribute('data', $answer) ;
    }

    public function testItemsIsJson()
    {

        $dpd = new DpdClient();
        $data = $dpd->getItems();
        $this->assertIsArray($data);
    }
}
